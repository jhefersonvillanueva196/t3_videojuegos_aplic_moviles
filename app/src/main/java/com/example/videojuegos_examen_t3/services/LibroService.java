package com.example.videojuegos_examen_t3.services;

import com.example.videojuegos_examen_t3.entities.Libro;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface LibroService {
    @GET("libros")
    Call<List<Libro>> getLibros();

    @POST("libros")
    Call<Libro> create(@Body Libro libro);
}
