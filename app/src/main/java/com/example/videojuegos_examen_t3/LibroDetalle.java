package com.example.videojuegos_examen_t3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.videojuegos_examen_t3.entities.Libro;
import com.google.gson.Gson;

public class LibroDetalle extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_libro);

        String contactJson = getIntent().getStringExtra("LIBRO");
        Libro libro = new Gson().fromJson(contactJson, Libro.class);

        ImageView image = findViewById(R.id.image);

        TextView tvTitulo = findViewById(R.id.tvTitulo);
        TextView tvResumen = findViewById(R.id.tvResumen);
        TextView tvAutor = findViewById(R.id.tvAutor);
        TextView tvFecha = findViewById(R.id.tvFecha);
        TextView tvTienda_1 = findViewById(R.id.tvTienda_1);
        TextView tvTienda_2 = findViewById(R.id.tvTienda_2);
        TextView tvTienda_3 = findViewById(R.id.tvTienda_3);

        byte[] decodedString = Base64.decode(libro.imagen, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        image.setImageBitmap(decodedByte);
        tvTitulo.setText("Titulo:" + libro.nombre);
        tvResumen.setText("Resumen: " + libro.resumen);
        tvAutor.setText("Autor: " + libro.autor);
        tvFecha.setText("Fecha de publicacion: " + libro.fecha_publicacion);
        tvTienda_1.setText("Tienda_1: " + libro.tienda_1);
        tvTienda_2.setText("Tienda_2: " + libro.tienda_2);
        tvTienda_3.setText("Tienda_3: " + libro.tienda_3);

        tvTienda_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                intent.putExtra("UBICACION", libro.tienda_1);
                startActivity(intent);
            }
        });

        tvTienda_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                intent.putExtra("UBICACION", libro.tienda_2);
                startActivity(intent);
            }
        });

        tvTienda_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                intent.putExtra("UBICACION", libro.tienda_3);
                startActivity(intent);
            }
        });

    }
}