package com.example.videojuegos_examen_t3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button nuevolibro = findViewById(R.id.NuevoLibro);
        Button listalibro = findViewById(R.id.ListaLibros);

        nuevolibro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),FormLibro.class);
                startActivity(intent);
            }
        });

        listalibro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),LibrosActivity.class);
                startActivity(intent);
            }
        });
    }
}