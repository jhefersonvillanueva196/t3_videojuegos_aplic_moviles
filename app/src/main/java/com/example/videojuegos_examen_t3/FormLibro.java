package com.example.videojuegos_examen_t3;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.videojuegos_examen_t3.entities.Libro;
import com.example.videojuegos_examen_t3.factories.RetrofitFactory;
import com.example.videojuegos_examen_t3.services.LibroService;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ListResourceBundle;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class FormLibro extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1000;
    static final int REQUEST_PICK_IMAGE = 1001;

    static final int REQUEST_CAMERA_PERMISSION = 100;

    ImageView image;
    String imagen;
    byte[] bytearray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_libro);

        image = findViewById(R.id.image);
        EditText titulo = findViewById(R.id.etNombre);
        EditText resumen = findViewById(R.id.etResumen);
        EditText autor = findViewById(R.id.etAutor);
        EditText fecha = findViewById(R.id.etFecha);

        Button tomarfoto = findViewById(R.id.TomarFoto);
        Button cargarimg = findViewById(R.id.CargarImagen);
        Button Crear = findViewById(R.id.Crear);

        tomarfoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tomarfotos();
            }
        });

        cargarimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirgaleria();
            }
        });

        Crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Libro libro = new Libro();
                libro.nombre = titulo.getText().toString();
                libro.resumen = resumen.getText().toString();
                libro.autor = autor.getText().toString();
                libro.fecha_publicacion = fecha.getText().toString();
                libro.tienda_1 = generarLonLat();
                libro.tienda_2 = generarLonLat();
                libro.tienda_3 = generarLonLat();
                libro.imagen = imagen;

                Retrofit retrofit = RetrofitFactory.build();
                LibroService service = retrofit.create(LibroService.class);

                Call<Libro> call = service.create(libro);

                call.enqueue(new Callback<Libro>() {
                    @Override
                    public void onResponse(Call<Libro> call, Response<Libro> response) {
                        if (response.isSuccessful()) {
                            Log.i("Crear", "Se creo correctamente");
                            Intent intent = new Intent(getApplicationContext(),LibrosActivity.class);
                            startActivity(intent);
                        } else {
                            Log.e("APP_VJ20202", "No se pudo eliminar el contacto");
                        }
                    }

                    @Override
                    public void onFailure(Call<Libro> call, Throwable t) {
                        Log.e("APP_VJ20202", "No nos podemos conectar al servicio web");
                    }
                });

            }
        });
    }

    private void tomarfotos() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
        } catch (ActivityNotFoundException e) {
            // display error state to the user
        }
    }

    private void abrirgaleria() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_PICK_IMAGE);
    }

    private String generarLonLat(){
        String cadena = "";
        double longitud = Math.random()*(180-(-180)+1)+(-180);
        double latitud = Math.random()*(90-(-90)+1)+(-90);
        cadena = longitud + ", " + latitud;
        return cadena;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK && data != null) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            image.setImageBitmap(imageBitmap);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
            imagen = Base64.encodeToString(byteArray, Base64.DEFAULT);
        }
        if (requestCode == REQUEST_PICK_IMAGE && resultCode == RESULT_OK && data != null) {
            try {
                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
                Bitmap imageBitmap = BitmapFactory.decodeStream(bufferedInputStream);
                image.setImageBitmap(imageBitmap);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream .toByteArray();
                imagen = Base64.encodeToString(byteArray, Base64.DEFAULT);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}