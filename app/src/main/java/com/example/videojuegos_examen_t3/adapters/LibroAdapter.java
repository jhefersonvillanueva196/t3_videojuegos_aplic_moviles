package com.example.videojuegos_examen_t3.adapters;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.videojuegos_examen_t3.LibroDetalle;
import com.example.videojuegos_examen_t3.R;
import com.example.videojuegos_examen_t3.entities.Libro;
import com.google.gson.Gson;

import java.util.List;

public class LibroAdapter extends RecyclerView.Adapter<LibroAdapter.LibroViewHolder>{

    List<Libro> libros;
    public LibroAdapter(List<Libro> libros) {
        this.libros = libros;
    }

    @NonNull
    @Override
    public LibroViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_item_libro, parent, false);
        return new LibroViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LibroViewHolder vh, int position) {

        View itemView = vh.itemView;

        Libro libro = libros.get(position);
        ImageView image = itemView.findViewById(R.id.image);
        TextView tvTitulo = itemView.findViewById(R.id.tvTitulo);

        byte[] decodedString = Base64.decode(libro.imagen, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        if(decodedByte!=null){
            image.setImageBitmap(decodedByte);
        }
        tvTitulo.setText("Titulo: " + libro.nombre);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(itemView.getContext(), LibroDetalle.class);

                String contactJSON = new Gson().toJson(libro);
                intent.putExtra("LIBRO", contactJSON);

                itemView.getContext().startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return libros.size();
    }

    class LibroViewHolder extends RecyclerView.ViewHolder implements AdapterView.OnItemClickListener {

        public LibroViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Libro contact = libros.get(i);
        }
    }
}

